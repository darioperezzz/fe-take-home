import React, { useContext } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import './app.css';
import {
  EuiFlyout,
  EuiFlyoutHeader,
  EuiTitle,
  EuiFlyoutBody,
  EuiButton,
  EuiFlyoutFooter,
  EuiFlexGroup,
  EuiFlexItem,
} from '@elastic/eui';
import HomePage from './pages/HomePage';
import ProductDetailsPage from './pages/ProductDetailsPage';
import ShoppingCart from './components/ShoppingCart';
import { StoreContext } from './commons/StoreContext';
import { closeCart } from './commons/cartControl';
import { formattedPrice, getTotalItems } from './utils';
import JIBEFooter from './components/JIBEFooter';

export default function App() {
  const [store, setStore] = useContext(StoreContext);
  let totalPrice = 0;
  store.shoppingCart.forEach((p) => (totalPrice += p.sale_price * p.amount));

  return (
    <Router>
      <Switch>
        <Route path="/products/:id">
          <ProductDetailsPage />
        </Route>
        <Route path="/">
          <HomePage />
        </Route>
      </Switch>

      <JIBEFooter />

      {store.showCart && (
        <EuiFlyout onClose={() => closeCart(store, setStore)}>
          <EuiFlyoutHeader hasBorder aria-labelledby="Shopping Cart">
            <EuiTitle>
              <h2>Shopping Cart</h2>
            </EuiTitle>
          </EuiFlyoutHeader>

          <EuiFlyoutBody>
            <ShoppingCart />
          </EuiFlyoutBody>

          <EuiFlyoutFooter>
            <EuiFlexGroup justifyContent="spaceBetween" alignItems="center">
              <EuiFlexItem grow={false}>
                <EuiTitle size="xs">
                  <div>{formattedPrice(totalPrice)} Total</div>
                </EuiTitle>
              </EuiFlexItem>
              <EuiFlexItem grow={false}>
                <EuiButton fill>
                  Buy ({getTotalItems(store.shoppingCart)} Items)
                </EuiButton>
              </EuiFlexItem>
            </EuiFlexGroup>
          </EuiFlyoutFooter>
        </EuiFlyout>
      )}
    </Router>
  );
}
