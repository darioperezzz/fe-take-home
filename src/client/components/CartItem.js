import React from 'react';
import { Link } from 'react-router-dom';
import {
  EuiFlexItem,
  EuiImage,
  EuiTitle,
  EuiBetaBadge,
  EuiBadge,
  EuiAspectRatio,
  EuiFlexGroup,
  EuiSpacer,
} from '@elastic/eui';
import './ProductItem.css';
import { getDiscountPercentage, formattedPrice } from '../utils';

export default function CartItem({ product }) {
  const { id, thumbnail, price, sale_price, title, in_stock, amount } = product;
  const discount = getDiscountPercentage(price, sale_price);
  return (
    <EuiFlexItem key={id}>
      <article className="jb-product jb-product--list-item">
        <EuiFlexGroup gutterSize="none" alignItems="center" responsive={false}>
          <EuiFlexItem grow={1}>
            <EuiAspectRatio width={1} height={1}>
              <EuiImage src={thumbnail} alt={title} />
            </EuiAspectRatio>
          </EuiFlexItem>
          <EuiFlexItem grow={3}>
            <section className="jb-product__body">
              <EuiTitle size="xs">
                <h1>
                  {in_stock === 'No' && (
                    <EuiBadge className="jb-mr-s">Out of Stock</EuiBadge>
                  )}
                  {title}
                </h1>
              </EuiTitle>

              <EuiTitle>
                <p>
                  {formattedPrice(sale_price)}
                  {discount > 0 && (
                    <EuiBetaBadge
                      className="jb-sale-tag jb-ml-xs"
                      color="accent"
                      label={`-${discount}%`}
                    />
                  )}
                </p>
              </EuiTitle>
              <EuiTitle size="xs">
                <p>{amount} item(s)</p>
              </EuiTitle>
            </section>
          </EuiFlexItem>
        </EuiFlexGroup>
      </article>
    </EuiFlexItem>
  );
}
