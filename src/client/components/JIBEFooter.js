import { EuiFlexGroup, EuiFlexItem } from '@elastic/eui';
import React from 'react';
import JIBENavLogo from './JIBENavLogo';

export default function JIBEFooter() {
  return (
    <footer
      className="jb-footer"
      style={{
        marginTop: '5rem',
      }}
    >
      <EuiFlexGroup
        alignItems="center"
        justifyContent="center"
        gutterSize="none"
        style={{
          backgroundColor: '#30d998',
          height: '10rem',
        }}
      >
        <EuiFlexItem grow={false}>
          <JIBENavLogo />
        </EuiFlexItem>
      </EuiFlexGroup>
    </footer>
  );
}
