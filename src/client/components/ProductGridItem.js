import React from 'react';
import { Link } from 'react-router-dom';
import {
  EuiFlexItem,
  EuiImage,
  EuiTitle,
  EuiBetaBadge,
  EuiBadge,
  EuiAspectRatio,
  EuiSpacer,
} from '@elastic/eui';
import './ProductItem.css';
import { getDiscountPercentage, formattedPrice } from '../utils';

export default function ProductGridItem({ product }) {
  const { id, thumbnail, price, sale_price, title, in_stock } = product;
  const discount = getDiscountPercentage(price, sale_price);
  return (
    <EuiFlexItem key={id}>
      <Link to={`/products/${id}`}>
        <article className="jb-product jb-product--grid-item">
          <EuiAspectRatio width={1} height={1}>
            <EuiImage src={thumbnail} alt={title} />
          </EuiAspectRatio>

          <section className="jb-product__body">
            <EuiTitle size="xs">
              <h1>
                {in_stock === 'No' && (
                  <EuiBadge className="jb-mr-s">Out of Stock</EuiBadge>
                )}
                {title}
              </h1>
            </EuiTitle>

            <EuiTitle size="m">
              <p>
                {formattedPrice(sale_price)}
                {discount > 0 && (
                  <EuiBetaBadge
                    className="jb-sale-tag jb-ml-xs"
                    color="accent"
                    label={`-${discount}%`}
                  />
                )}
              </p>
            </EuiTitle>
          </section>
        </article>
      </Link>
    </EuiFlexItem>
  );
}
