import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import {
  EuiHeader,
  EuiHeaderSectionItem,
  EuiPanel,
  EuiFlexGroup,
  EuiFlexItem,
  EuiImage,
  EuiTitle,
  EuiPage,
  EuiPageBody,
  EuiEmptyPrompt,
  EuiButton,
  EuiPageContent,
} from '@elastic/eui';
import { StoreContext } from '../commons/StoreContext';
import JIBENavLogo from './JIBENavLogo';
import { closeCart } from '../commons/cartControl';
import CartItem from './CartItem';

function renderCartItems(cartItems) {
  return <EuiPageBody>{cartItems}</EuiPageBody>;
}

function renderEmptyState(store, setStore) {
  return (
    <EuiPageBody>
      <EuiPageContent
        verticalPosition="center"
        horizontalPosition="center"
        hasShadow={false}
      >
        <EuiEmptyPrompt
          iconType="package"
          title={<h2>You have no Items in your Cart yet</h2>}
          actions={
            <EuiButton onClick={() => closeCart(store, setStore)}>
              Show me the store!
            </EuiButton>
          }
        />
      </EuiPageContent>
    </EuiPageBody>
  );
}

export default function ShoppingCart(props) {
  const [store, setStore] = useContext(StoreContext);
  const cartItems = store?.shoppingCart.map((p) => (
    <CartItem key={p.id} product={p} />
  ));
  return (
    <>
      {store.shoppingCart.length < 1 && renderEmptyState(store, setStore)}

      {store.shoppingCart.length > 0 && renderCartItems(cartItems)}
    </>
  );
}
