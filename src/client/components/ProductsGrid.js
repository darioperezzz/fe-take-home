import React from 'react';
import {
  EuiFlexGrid
} from '@elastic/eui';
import ProductGridItem from './ProductGridItem';

export default function ProductsGrid({ products }) {
  return (
    <EuiFlexGrid columns={3}>
      {products &&
        products.map((product) => (
          <ProductGridItem key={product.id} product={product} />
        ))}
    </EuiFlexGrid>
  );
};