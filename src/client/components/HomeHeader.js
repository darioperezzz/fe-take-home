import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import {
  EuiHeader,
  EuiHeaderSection,
  EuiHeaderSectionItem,
  EuiFieldSearch,
  EuiHeaderSectionItemButton,
  EuiSelect,
  EuiButtonIcon,
  EuiButtonGroup,
  EuiButton,
  EuiFlexGroup,
  EuiFlexItem,
  EuiImage,
} from '@elastic/eui';
import JIBENavLogo from './JIBENavLogo';
import { ViewMode } from '../commons/constants';

function formatSelectValues(values) {
  const formattedValues = [
    {
      value: 'Any',
      text: 'Any',
    },
  ];
  values.forEach((e) =>
    formattedValues.push({
      value: e,
      text: e,
    })
  );
  return formattedValues;
}

export default function HomeHeader({
  categories,
  sizes,
  colors,
  viewMode,
  sortOption,
  onFiltersUpdate,
  onSearchUpdate,
  onViewModeChange,
  onShowCart,
}) {
  const [filters, setFilters] = useState({
    category: categories[0]?.value || 'Any',
    color: colors[0]?.value || 'Any',
    size: sizes[0]?.value || 'Any',
    sortOption: sortOption || 'title',
  });

  const handleFilterChange = (filter, value) => {
    const newFilters = { ...filters };
    newFilters[filter] = value;
    setFilters(newFilters);
    onFiltersUpdate(newFilters);
  };

  const handleSearchChange = (e) => {
    onSearchUpdate(e.target.value);
  };

  const handleViewModeChange = (id) => {
    onViewModeChange(id);
  };

  return (
    <>
      {/* Header */}
      <EuiHeader style={{ borderBottom: 'none' }}>
        <EuiHeaderSection grow={false}>
          <EuiHeaderSectionItem>
            <JIBENavLogo />
          </EuiHeaderSectionItem>
        </EuiHeaderSection>

        <EuiHeaderSection>
          <EuiHeaderSectionItem>
            <EuiFieldSearch
              isClearable
              onChange={handleSearchChange}
              placeholder="Search"
            />
          </EuiHeaderSectionItem>
        </EuiHeaderSection>

        <EuiHeaderSection side="right">
          <EuiHeaderSectionItem>
            {/* <Link to="/cart">
              <EuiButton iconType="package">Review Cart</EuiButton>
            </Link> */}
            <EuiButton iconType="package" onClick={onShowCart}>
              Review Cart
            </EuiButton>
          </EuiHeaderSectionItem>
        </EuiHeaderSection>
      </EuiHeader>

      {/* Subheader: Filters and View Mode */}
      <EuiHeader>
        <EuiHeaderSection grow={false}>
          <EuiHeaderSectionItem>
            <EuiFlexGroup gutterSize="s">
              <EuiFlexItem>
                <EuiSelect
                  options={formatSelectValues(categories)}
                  onChange={(e) =>
                    handleFilterChange('category', e.target.value)
                  }
                  value={filters.category}
                  prepend="Category"
                />
              </EuiFlexItem>
              <EuiFlexItem>
                <EuiSelect
                  options={formatSelectValues(sizes)}
                  onChange={(e) => handleFilterChange('size', e.target.value)}
                  value={filters.size}
                  prepend="Size"
                />
              </EuiFlexItem>
              <EuiFlexItem>
                <EuiSelect
                  options={formatSelectValues(colors)}
                  onChange={(e) => handleFilterChange('color', e.target.value)}
                  value={filters.color}
                  prepend="Color"
                />
              </EuiFlexItem>
              <EuiFlexItem>
                <EuiButton
                  iconType="sortable"
                  color="text"
                  onClick={(e) =>
                    handleFilterChange(
                      'sortOption',
                      sortOption === 'title' ? 'sale_price' : 'title'
                    )
                  }
                >
                  Sort by {sortOption === 'sale_price' ? 'Name' : 'Price'}
                </EuiButton>
              </EuiFlexItem>
            </EuiFlexGroup>
          </EuiHeaderSectionItem>
        </EuiHeaderSection>

        <EuiHeaderSection side="right">
          <EuiHeaderSectionItem>
            <EuiButtonGroup
              legend="Products View Mode"
              isIconOnly
              name="jb-view-mode"
              idSelected={viewMode}
              options={[
                {
                  iconType: 'grid',
                  id: ViewMode.grid,
                  label: 'View as Grid',
                },
                {
                  iconType: 'list',
                  id: ViewMode.list,
                  label: 'View as List',
                },
              ]}
              onChange={handleViewModeChange}
            />
          </EuiHeaderSectionItem>
        </EuiHeaderSection>
      </EuiHeader>
    </>
  );
}
