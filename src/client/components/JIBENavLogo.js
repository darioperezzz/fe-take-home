import React from 'react';
import { EuiImage } from '@elastic/eui';
import JIBELogo from '../jibe-logo.svg';

export default function name() {
  return <EuiImage className="jb-nav-logo" src={JIBELogo} alt="JIBE Logo" />;
}
