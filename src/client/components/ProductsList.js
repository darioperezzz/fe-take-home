import React from 'react';
import {
  EuiPageContent,
  EuiPageContentBody
} from '@elastic/eui';
import ProductListItem from './ProductListItem';

export default function ProductsList({ products }) {
  return (
    <EuiPageContent
      hasBorder={false}
      hasShadow={false}
      paddingSize="none"
      color="transparent"
      borderRadius="none"
    >
      <EuiPageContentBody>
        {products &&
          products.map((product) => (
            <ProductListItem key={product.id} product={product} />
          ))}
      </EuiPageContentBody>
    </EuiPageContent>
  );
};