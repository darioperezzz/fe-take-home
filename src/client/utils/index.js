export function getDiscountPercentage(price, salePrice) {
  return 100 - (salePrice * 100) / price;
}

export function isOutOfStock(product) {
  return product.in_stock === 'No';
};

export function getFilteredProducts(query, products) {

  if (query.length > 0) {
    return products.filter((p) =>
      p.title.toUpperCase().includes(query.toUpperCase())
    );
  }

  return products;
};

export function getCategories (products) {
  const newCategories = new Set();
  products.forEach((p) => p.categories.forEach((c) => newCategories.add(c)));
  return newCategories.toJSON().sort();
};

export function getSizes (products) {
  const newSizes = new Set();
  products.forEach((p) => p.sizes.forEach((s) => newSizes.add(s)));
  return newSizes.toJSON().sort();
};

export function getColors (products) {
  const newColors = new Set();
  products.forEach((p) => newColors.add(p.color));
  return newColors.toJSON().sort();
};

export function normalizePrices(products) {
  return products.map((p) => normalizePrice(p));
}

export function normalizePrice(product) {
  const newProduct = { ...product };
  newProduct.price = parseFloat(product.price.replace('$', ''));
  newProduct.sale_price = parseFloat(product.sale_price.replace('$', ''));
  return newProduct;
}

export function getTotalItems(items) {
  let result = 0;
  items.forEach((i) => (result += parseInt(i.amount)));
  return result;
}

export function formattedPrice(amount) {
  return `$${parseFloat(amount).toFixed(2)}`;
}