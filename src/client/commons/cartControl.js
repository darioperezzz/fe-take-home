export const closeCart = (store, setStore) => {
  const newStore = { ...store };
  newStore.showCart = false;
  setStore(newStore);
};

export const showCart = (store, setStore) => {
  const newStore = { ...store };
  newStore.showCart = true;
  setStore(newStore);
};
