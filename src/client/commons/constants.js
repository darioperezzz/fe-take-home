export const ViewMode = {
  grid: 'jb-view-mode--grid',
  list: 'jb-view-mode--list',
};
