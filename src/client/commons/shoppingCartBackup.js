const LS_STORE_KEY = 'the-jibe-store-backup';

export function getShoppingCartBackup() {
  return JSON.parse(localStorage.getItem(LS_STORE_KEY));
}

export function setShoppingCartBackup(shoppingCart) {
  return localStorage.setItem(LS_STORE_KEY, JSON.stringify(shoppingCart));
}
