import React, { useEffect, useState } from 'react';
import {
  getShoppingCartBackup,
  setShoppingCartBackup,
} from './shoppingCartBackup';

const storeContentContext = {
  products: [],
  shoppingCart: [],
  categories: [],
  sizes: [],
  colors: [],
  showCart: false,
};

export const StoreContext = React.createContext(storeContentContext);

export const StoreContextProvider = ({ children }) => {
  const [store, setStore] = useState(storeContentContext);

  useEffect(() => {
    const backup = getShoppingCartBackup();
    if (backup) {
      const newStore = { ...store };
      newStore.shoppingCart = getShoppingCartBackup();
      setStore(newStore);
    }
  }, []);

  useEffect(() => {
    setShoppingCartBackup(store.shoppingCart);
  }, [store.shoppingCart]);

  return (
    <StoreContext.Provider value={[store, setStore]}>
      {children}
    </StoreContext.Provider>
  );
};
