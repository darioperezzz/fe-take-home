import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import '@elastic/eui/dist/eui_theme_amsterdam_light.css';
import { StoreContextProvider } from './commons/StoreContext';

ReactDOM.render(
  <StoreContextProvider>
    <App />
  </StoreContextProvider>,
  document.getElementById('root')
);
