import React, { useEffect, useState, useContext } from 'react';
import {
  EuiText,
  EuiHeader,
  EuiHeaderSection,
  EuiHeaderSectionItem,
  EuiButton,
  EuiImage,
  EuiTitle,
  EuiFlexItem,
  EuiFlexGroup,
  EuiBadge,
  EuiSuperSelect,
  EuiFieldNumber,
  EuiPage,
  EuiPageBody,
  EuiCallOut,
  EuiGlobalToastList,
  EuiPageContent,
  EuiSpacer,
  EuiBetaBadge,
  EuiFormRow,
  EuiForm,
} from '@elastic/eui';
import { appendIconComponentCache } from '@elastic/eui/es/components/icon/icon';

import { icon as EuiIconFullScreen } from '@elastic/eui/es/components/icon/assets/full_screen';
import { icon as EuiIconCross } from '@elastic/eui/es/components/icon/assets/cross';
import { icon as EuiIconCheck } from '@elastic/eui/es/components/icon/assets/check';
import { icon as EuiIconFlag } from '@elastic/eui/es/components/icon/assets/flag';

import { Link, useParams } from 'react-router-dom';
import { StoreContext } from '../commons/StoreContext';
import {
  formattedPrice,
  getDiscountPercentage,
  isOutOfStock,
  normalizePrice,
} from '../utils';
import JIBENavLogo from '../components/JIBENavLogo';
import { showCart } from '../commons/cartControl';

let toastId = 0;

appendIconComponentCache({
  fullscreen: EuiIconFullScreen,
  cross: EuiIconCross,
  check: EuiIconCheck,
  flag: EuiIconFlag,
});

export default function ProductDetailsPage() {
  const [store, setStore] = useContext(StoreContext);
  const { id } = useParams();
  const [currentProduct, setCurrentProduct] = useState(null);
  const [size, setSize] = useState('');
  const [amount, setAmount] = useState(1);
  const [toasts, setToasts] = useState([]);

  const addToast = (toast) => {
    setToasts(toasts.concat(toast));
  };

  const notifyInStockFeedback = () => {
    addToast({
      id: `jb-cart-feedback-${++toastId}`,
      title: 'Noted!',
      text: "We'll let you know when it's back in stock",
    });
  };

  const addProduct = () => {
    const newShoppingCart = [...store.shoppingCart];
    let feedbackMessage;
    const productIndex = newShoppingCart.findIndex(
      (p) => p.id === currentProduct.id
    );
    const productInCart = productIndex > -1;
    if (productInCart) {
      newShoppingCart[productIndex] = currentProduct;
      newShoppingCart[productIndex].amount = amount;
      feedbackMessage = '👍 Product Updated in Cart';
    } else {
      currentProduct.amount = amount;
      newShoppingCart.push(currentProduct);
      feedbackMessage = '👍 Product Added to Cart';
    }

    const newStore = { ...store };
    newStore.shoppingCart = newShoppingCart;
    setStore(newStore);
    addToast({
      id: `jb-cart-feedback-${++toastId}`,
      title: feedbackMessage,
      color: 'success',
    });
  };

  useEffect(() => {
    if (!currentProduct) {
      const fetchProduct = async (productId) => {
        const response = await fetch(`/api/products/${productId}`);
        const newProduct = normalizePrice(await response.json());
        setCurrentProduct(newProduct);
        setSize(newProduct.sizes[0]);
      };

      fetchProduct(id);
    }
  }, []);

  const onSizeChange = (value) => {
    setSize(value);
  };

  const onAmountChange = (e) => {
    setAmount(e.target.value);
  };

  if (currentProduct) {
    const uniqueSizes = new Set();
    const sizes = [];
    const discount = getDiscountPercentage(
      currentProduct.price,
      currentProduct.sale_price
    );

    currentProduct.sizes.forEach((s) => uniqueSizes.add(s));
    uniqueSizes.forEach((s) =>
      sizes.push({
        value: s,
        inputDisplay: s,
      })
    );

    return (
      <>
        <ProductDetailsHeader onShowCart={() => showCart(store, setStore)} />

        <EuiPage restrictWidth="1024px">
          <EuiPageBody>
            <div>
              <Link to="/">
                <EuiButton color="text">Go Back</EuiButton>
              </Link>
            </div>

            <EuiSpacer size="l" />

            <EuiPageContent>
              <EuiFlexGroup>
                <EuiFlexItem>
                  <ProductGallery
                    images={currentProduct.images}
                    title={currentProduct.title}
                  />
                </EuiFlexItem>

                <EuiFlexItem>
                  <EuiTitle size="l">
                    <h2>{currentProduct.title}</h2>
                  </EuiTitle>

                  <EuiTitle size="s">
                    <p>
                      {formattedPrice(currentProduct.sale_price)}{' '}
                      {discount > 0 && (
                        <EuiBetaBadge
                          className="jb-sale-tag jb-ml-xs"
                          color="accent"
                          label={`-${discount}%`}
                        />
                      )}
                    </p>
                  </EuiTitle>

                  <EuiSpacer size="l" />
                  <ProductCategories categories={currentProduct.categories} />
                  <EuiSpacer size="l" />

                  {!isOutOfStock(currentProduct) && (
                    <EuiForm>
                      <EuiFormRow label="Select Size">
                        <EuiSuperSelect
                          options={sizes}
                          valueOfSelected={size}
                          onChange={onSizeChange}
                        />
                      </EuiFormRow>
                      <EuiFormRow label="Select Amount">
                        <EuiFieldNumber
                          placeholder="Amount"
                          min={1}
                          value={amount}
                          onChange={onAmountChange}
                        />
                      </EuiFormRow>
                      <EuiButton fill onClick={addProduct}>
                        Add to Cart
                      </EuiButton>
                    </EuiForm>
                  )}

                  {isOutOfStock(currentProduct) && (
                    <>
                      <EuiCallOut
                        iconType="flag"
                        title="This product is out of stock at the moment."
                        color="primary"
                      />
                      <EuiSpacer size="l" />
                      <EuiButton onClick={notifyInStockFeedback}>
                        Notify me when it's back in stock
                      </EuiButton>
                    </>
                  )}
                </EuiFlexItem>
              </EuiFlexGroup>
            </EuiPageContent>
          </EuiPageBody>
          <EuiGlobalToastList toasts={toasts} toastLifeTimeMs={3000} />
        </EuiPage>
      </>
    );
  }

  return <EuiText>Loading...</EuiText>;
}

function ProductDetailsHeader({ onShowCart }) {
  return (
    <EuiHeader>
      <EuiHeaderSection grow={false}>
        <EuiHeaderSectionItem>
          <JIBENavLogo />
        </EuiHeaderSectionItem>
      </EuiHeaderSection>

      <EuiHeaderSection>
        <EuiHeaderSectionItem>
          <EuiButton iconType="package" onClick={onShowCart}>
            Review Cart
          </EuiButton>
        </EuiHeaderSectionItem>
      </EuiHeaderSection>
    </EuiHeader>
  );
}

function ProductGallery({ title, images }) {
  return (
    <EuiFlexGroup>
      {images.map((image, key) => (
        <EuiFlexItem key={key}>
          <EuiImage src={image.url} alt={title} allowFullScreen />
        </EuiFlexItem>
      ))}
    </EuiFlexGroup>
  );
}

function ProductCategories({ categories }) {
  return (
    <p>
      {categories.map((c, i) => (
        <EuiBadge key={i} color="hollow">
          {c}
        </EuiBadge>
      ))}
    </p>
  );
}
