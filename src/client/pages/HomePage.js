import React, { useContext, useEffect, useState } from 'react';
import {
  EuiEmptyPrompt,
  EuiPage,
  EuiPageBody,
  EuiSpacer,
  EuiTitle,
} from '@elastic/eui';
import { StoreContext } from '../commons/StoreContext';
import HomeHeader from '../components/HomeHeader';
import ProductsGrid from '../components/ProductsGrid';
import ProductsList from '../components/ProductsList';
import { ViewMode } from '../commons/constants';
import { showCart } from '../commons/cartControl';
import {
  getCategories,
  getColors,
  getFilteredProducts,
  getSizes,
  normalizePrices,
} from '../utils';

const renderProducts = (viewMode, products) => {
  if (products && products.length > 0) {
    return viewMode === ViewMode.grid ? (
      <ProductsGrid products={products} />
    ) : (
      <ProductsList products={products} />
    );
  }

  return (
    <EuiEmptyPrompt
      iconType="stop"
      title={<h2>Nothing found</h2>}
      body={<p>It seems like we don't have that at the moment.</p>}
    />
  );
};

export default function HomePage() {
  const [products, setProducts] = useState(null);
  const [store, setStore] = useContext(StoreContext);
  const [searchQuery, setSearchQuery] = useState('');
  const [viewMode, setViewMode] = useState(ViewMode.grid);
  const [sortOption, setSortOption] = useState('title');
  let loading = true;

  const searchInProducts = (query) => {
    const searchQuery = query.trim();
    setSearchQuery(searchQuery);

    if (searchQuery.length > 0) {
      const newProducts = getFilteredProducts(searchQuery, store.products);

      setProducts(newProducts);
    } else {
      setProducts(store.products);
    }
  };

  const fetchProducts = async ({
    category = 'Any',
    size = 'Any',
    color = 'Any',
    sortOption = 'title',
  }) => {
    loading = true;
    const url = '/api/products?';
    const searchParams = {};

    // Set Filters

    if (category && category !== 'Any') {
      searchParams.category = category;
    }
    if (size && size !== 'Any') {
      searchParams.size = size;
    }
    if (color && color !== 'Any') {
      searchParams.color = color;
    }

    // Fetch with desired Filters
    const response = await fetch(url + new URLSearchParams(searchParams));
    let newProducts = await response.json();
    newProducts = normalizePrices(newProducts);
    newProducts = getFilteredProducts(searchQuery, newProducts);

    // Sort Products
    setSortOption(sortOption);
    newProducts = newProducts.sort((a, b) => {
      if (a[sortOption] > b[sortOption]) {
        return 1;
      }

      if (a[sortOption] < b[sortOption]) {
        return -1;
      }

      return 0;
    });
    setProducts(newProducts);

    // Set Categories, Sizes, and Colors for Filter Selects
    const newStore = { ...store };
    newStore.products = newProducts;

    if (newStore.categories.length <= 0) {
      newStore.categories = getCategories(newProducts);
    }
    if (newStore.sizes.length <= 0) {
      newStore.sizes = getSizes(newProducts);
    }
    if (newStore.colors.length <= 0) {
      newStore.colors = getColors(newProducts);
    }

    // Update Store Context
    setStore(newStore);
    loading = false;
  };

  // Fetch Products the First Time
  useEffect(() => {
    if (!products) {
      fetchProducts({});
    }
  });

  return (
    <>
      <HomeHeader
        categories={store.categories}
        sizes={store.sizes}
        colors={store.colors}
        viewMode={viewMode}
        sortOption={sortOption}
        onFiltersUpdate={fetchProducts}
        onSearchUpdate={searchInProducts}
        onViewModeChange={(viewMode) => setViewMode(viewMode)}
        onShowCart={() => {
          showCart(store, setStore);
        }}
      />

      <EuiPage restrictWidth={viewMode === ViewMode.grid ? '1024px' : '640px'}>
        <EuiPageBody>
          <EuiTitle>
            <p>Only the best quality & service</p>
          </EuiTitle>

          <EuiSpacer size="l" />

          {renderProducts(viewMode, products)}
        </EuiPageBody>
      </EuiPage>
    </>
  );
}
