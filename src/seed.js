function products() {
  return [
    {
      id: 0,
      title: 'Product #0',
      price: '$1.99',
      sale_price: '$1.4925',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/342x248/f8f9fa/495057?text=PRODUCT%20%230',
      images: [
        {
          url: 'https://via.placeholder.com/568x813/f8f9fa/495057?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/1329x1022/ffa8a8/fff5f5?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/790x1071/202846/edf2ff?text=IMAGE%203',
        },
      ],
      categories: ['Pants', 'Socks', 'Sweaters'],
      color: 'Blue',
      sizes: ['M', 'M', 'XS'],
    },
    {
      id: 1,
      title: 'Product #1',
      price: '$24000.99',
      sale_price: '$24000.99',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/272x268/ffa8a8/fff5f5?text=PRODUCT%20%231',
      images: [
        {
          url: 'https://via.placeholder.com/1269x714/202846/edf2ff?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/575x1407/f8f9fa/495057?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/493x915/ffa8a8/fff5f5?text=IMAGE%203',
        },
      ],
      categories: ['Socks', 'Caps', 'Pants'],
      color: 'Blue',
      sizes: ['Large', 'Small', 'XL'],
    },
    {
      id: 2,
      title: 'Product #2',
      price: '$9.99',
      sale_price: '$7.4925',
      in_stock: 'No',
      thumbnail:
        'https://via.placeholder.com/340x476/30d998/fff9db?text=PRODUCT%20%232',
      images: [
        {
          url: 'https://via.placeholder.com/655x927/f8f9fa/495057?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/803x615/202846/edf2ff?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/1294x948/202846/edf2ff?text=IMAGE%203',
        },
      ],
      categories: ['Pants', 'Socks', 'Sweaters'],
      color: 'Black',
      sizes: ['Small', 'XS', 'XS'],
    },
    {
      id: 3,
      title: 'Product #3',
      price: '$0.99',
      sale_price: '$0.99',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/284x272/ffa8a8/fff5f5?text=PRODUCT%20%233',
      images: [
        {
          url: 'https://via.placeholder.com/622x742/202846/edf2ff?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/622x1439/30d998/fff9db?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/1353x1255/ffa8a8/fff5f5?text=IMAGE%203',
        },
      ],
      categories: ['Pants', 'Sweaters', 'Socks'],
      color: 'White',
      sizes: ['XL', 'Small', 'M'],
    },
    {
      id: 4,
      title: 'Product #4',
      price: '$0.99',
      sale_price: '$0.99',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/449x370/30d998/fff9db?text=PRODUCT%20%234',
      images: [
        {
          url: 'https://via.placeholder.com/1365x1396/202846/edf2ff?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/1003x914/202846/edf2ff?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/689x907/202846/edf2ff?text=IMAGE%203',
        },
      ],
      categories: ['Sweaters', 'Jackets', 'Pants'],
      color: 'White',
      sizes: ['Small', 'XS', 'M'],
    },
    {
      id: 5,
      title: 'Product #5',
      price: '$1.99',
      sale_price: '$1.99',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/307x296/30d998/fff9db?text=PRODUCT%20%235',
      images: [
        {
          url: 'https://via.placeholder.com/920x684/202846/edf2ff?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/1102x584/f8f9fa/495057?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/1435x1356/f8f9fa/495057?text=IMAGE%203',
        },
      ],
      categories: ['Pants', 'Jackets', 'Caps'],
      color: 'Black',
      sizes: ['Small', 'XL', 'XS'],
    },
    {
      id: 6,
      title: 'Product #6',
      price: '$9.99',
      sale_price: '$9.99',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/471x287/202846/edf2ff?text=PRODUCT%20%236',
      images: [
        {
          url: 'https://via.placeholder.com/699x793/ffa8a8/fff5f5?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/1202x496/202846/edf2ff?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/520x938/30d998/fff9db?text=IMAGE%203',
        },
      ],
      categories: ['Socks', 'Socks', 'Sweaters'],
      color: 'White',
      sizes: ['M', 'M', 'Large'],
    },
    {
      id: 7,
      title: 'Product #7',
      price: '$24000.99',
      sale_price: '$24000.99',
      in_stock: 'No',
      thumbnail:
        'https://via.placeholder.com/292x344/f8f9fa/495057?text=PRODUCT%20%237',
      images: [
        {
          url: 'https://via.placeholder.com/816x553/202846/edf2ff?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/1282x908/f8f9fa/495057?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/852x642/202846/edf2ff?text=IMAGE%203',
        },
      ],
      categories: ['Pants', 'Caps', 'Sweaters'],
      color: 'White',
      sizes: ['M', 'Large', 'Small'],
    },
    {
      id: 8,
      title: 'Product #8',
      price: '$12.49',
      sale_price: '$12.49',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/418x310/f8f9fa/495057?text=PRODUCT%20%238',
      images: [
        {
          url: 'https://via.placeholder.com/875x540/202846/edf2ff?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/694x545/202846/edf2ff?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/884x1405/202846/edf2ff?text=IMAGE%203',
        },
      ],
      categories: ['Socks', 'Pants', 'Jackets'],
      color: 'White',
      sizes: ['Small', 'Large', 'Large'],
    },
    {
      id: 9,
      title: 'Product #9',
      price: '$9.99',
      sale_price: '$9.99',
      in_stock: 'No',
      thumbnail:
        'https://via.placeholder.com/274x249/202846/edf2ff?text=PRODUCT%20%239',
      images: [
        {
          url: 'https://via.placeholder.com/811x627/30d998/fff9db?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/818x903/f8f9fa/495057?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/644x818/f8f9fa/495057?text=IMAGE%203',
        },
      ],
      categories: ['Caps', 'Caps', 'Caps'],
      color: 'White',
      sizes: ['XS', 'Small', 'M'],
    },
    {
      id: 10,
      title: 'Product #10',
      price: '$24000.99',
      sale_price: '$24000.99',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/441x442/f8f9fa/495057?text=PRODUCT%20%2310',
      images: [
        {
          url: 'https://via.placeholder.com/958x1346/30d998/fff9db?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/719x1381/ffa8a8/fff5f5?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/749x1138/202846/edf2ff?text=IMAGE%203',
        },
      ],
      categories: ['Socks', 'Socks', 'Pants'],
      color: 'Black',
      sizes: ['XL', 'Small', 'XS'],
    },
    {
      id: 11,
      title: 'Product #11',
      price: '$24000.99',
      sale_price: '$24000.99',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/430x336/30d998/fff9db?text=PRODUCT%20%2311',
      images: [
        {
          url: 'https://via.placeholder.com/707x550/f8f9fa/495057?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/1131x833/ffa8a8/fff5f5?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/790x1354/f8f9fa/495057?text=IMAGE%203',
        },
      ],
      categories: ['Sweaters', 'Socks', 'Sweaters'],
      color: 'Blue',
      sizes: ['XS', 'XS', 'XS'],
    },
    {
      id: 12,
      title: 'Product #12',
      price: '$12.49',
      sale_price: '$12.49',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/280x381/ffa8a8/fff5f5?text=PRODUCT%20%2312',
      images: [
        {
          url: 'https://via.placeholder.com/661x1273/ffa8a8/fff5f5?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/1337x655/30d998/fff9db?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/490x1263/f8f9fa/495057?text=IMAGE%203',
        },
      ],
      categories: ['Sweaters', 'Sweaters', 'Jackets'],
      color: 'Yellow',
      sizes: ['Small', 'XS', 'M'],
    },
    {
      id: 13,
      title: 'Product #13',
      price: '$24000.99',
      sale_price: '$18000.7425',
      in_stock: 'No',
      thumbnail:
        'https://via.placeholder.com/379x470/f8f9fa/495057?text=PRODUCT%20%2313',
      images: [
        {
          url: 'https://via.placeholder.com/1216x636/202846/edf2ff?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/498x1028/ffa8a8/fff5f5?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/917x1404/ffa8a8/fff5f5?text=IMAGE%203',
        },
      ],
      categories: ['Sweaters', 'Pants', 'Pants'],
      color: 'Yellow',
      sizes: ['Small', 'XS', 'XL'],
    },
    {
      id: 14,
      title: 'Product #14',
      price: '$1.99',
      sale_price: '$1.99',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/406x440/ffa8a8/fff5f5?text=PRODUCT%20%2314',
      images: [
        {
          url: 'https://via.placeholder.com/1294x664/202846/edf2ff?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/1355x860/f8f9fa/495057?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/1407x814/202846/edf2ff?text=IMAGE%203',
        },
      ],
      categories: ['Pants', 'Pants', 'Socks'],
      color: 'Yellow',
      sizes: ['Small', 'XL', 'Large'],
    },
    {
      id: 15,
      title: 'Product #15',
      price: '$0.99',
      sale_price: '$0.99',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/284x336/ffa8a8/fff5f5?text=PRODUCT%20%2315',
      images: [
        {
          url: 'https://via.placeholder.com/659x901/202846/edf2ff?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/1070x705/202846/edf2ff?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/1200x787/ffa8a8/fff5f5?text=IMAGE%203',
        },
      ],
      categories: ['Pants', 'Jackets', 'Caps'],
      color: 'Yellow',
      sizes: ['Small', 'Large', 'Large'],
    },
    {
      id: 16,
      title: 'Product #16',
      price: '$0.99',
      sale_price: '$0.99',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/332x243/f8f9fa/495057?text=PRODUCT%20%2316',
      images: [
        {
          url: 'https://via.placeholder.com/1252x1162/f8f9fa/495057?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/1044x878/30d998/fff9db?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/1196x927/f8f9fa/495057?text=IMAGE%203',
        },
      ],
      categories: ['Jackets', 'Sweaters', 'Pants'],
      color: 'Blue',
      sizes: ['M', 'XL', 'XS'],
    },
    {
      id: 17,
      title: 'Product #17',
      price: '$1.99',
      sale_price: '$1.4925',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/308x395/ffa8a8/fff5f5?text=PRODUCT%20%2317',
      images: [
        {
          url: 'https://via.placeholder.com/1280x1115/f8f9fa/495057?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/557x799/202846/edf2ff?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/909x576/f8f9fa/495057?text=IMAGE%203',
        },
      ],
      categories: ['Caps', 'Jackets', 'Sweaters'],
      color: 'Black',
      sizes: ['Large', 'Small', 'XL'],
    },
    {
      id: 18,
      title: 'Product #18',
      price: '$0.99',
      sale_price: '$0.99',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/354x253/f8f9fa/495057?text=PRODUCT%20%2318',
      images: [
        {
          url: 'https://via.placeholder.com/1147x848/ffa8a8/fff5f5?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/1291x868/30d998/fff9db?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/608x834/30d998/fff9db?text=IMAGE%203',
        },
      ],
      categories: ['Caps', 'Pants', 'Socks'],
      color: 'White',
      sizes: ['M', 'Large', 'M'],
    },
    {
      id: 19,
      title: 'Product #19',
      price: '$1.99',
      sale_price: '$1.4925',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/375x357/30d998/fff9db?text=PRODUCT%20%2319',
      images: [
        {
          url: 'https://via.placeholder.com/571x1407/ffa8a8/fff5f5?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/612x547/30d998/fff9db?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/633x629/f8f9fa/495057?text=IMAGE%203',
        },
      ],
      categories: ['Caps', 'Socks', 'Jackets'],
      color: 'Blue',
      sizes: ['M', 'XS', 'Large'],
    },
    {
      id: 20,
      title: 'Product #20',
      price: '$9.99',
      sale_price: '$7.4925',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/283x245/ffa8a8/fff5f5?text=PRODUCT%20%2320',
      images: [
        {
          url: 'https://via.placeholder.com/981x483/ffa8a8/fff5f5?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/647x898/30d998/fff9db?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/986x889/f8f9fa/495057?text=IMAGE%203',
        },
      ],
      categories: ['Pants', 'Socks', 'Jackets'],
      color: 'White',
      sizes: ['XS', 'M', 'XS'],
    },
    {
      id: 21,
      title: 'Product #21',
      price: '$24000.99',
      sale_price: '$24000.99',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/379x395/202846/edf2ff?text=PRODUCT%20%2321',
      images: [
        {
          url: 'https://via.placeholder.com/1220x890/ffa8a8/fff5f5?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/634x1394/ffa8a8/fff5f5?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/1346x1079/ffa8a8/fff5f5?text=IMAGE%203',
        },
      ],
      categories: ['Caps', 'Caps', 'Sweaters'],
      color: 'Black',
      sizes: ['XL', 'XS', 'XS'],
    },
    {
      id: 22,
      title: 'Product #22',
      price: '$1.99',
      sale_price: '$1.99',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/458x465/202846/edf2ff?text=PRODUCT%20%2322',
      images: [
        {
          url: 'https://via.placeholder.com/904x850/f8f9fa/495057?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/1008x1199/ffa8a8/fff5f5?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/644x1298/202846/edf2ff?text=IMAGE%203',
        },
      ],
      categories: ['Caps', 'Jackets', 'Jackets'],
      color: 'Blue',
      sizes: ['M', 'XL', 'Large'],
    },
    {
      id: 23,
      title: 'Product #23',
      price: '$24000.99',
      sale_price: '$24000.99',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/472x303/f8f9fa/495057?text=PRODUCT%20%2323',
      images: [
        {
          url: 'https://via.placeholder.com/1406x1290/ffa8a8/fff5f5?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/504x805/30d998/fff9db?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/1357x1338/202846/edf2ff?text=IMAGE%203',
        },
      ],
      categories: ['Sweaters', 'Pants', 'Sweaters'],
      color: 'Blue',
      sizes: ['Small', 'M', 'Large'],
    },
    {
      id: 24,
      title: 'Product #24',
      price: '$9.99',
      sale_price: '$9.99',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/368x441/202846/edf2ff?text=PRODUCT%20%2324',
      images: [
        {
          url: 'https://via.placeholder.com/1101x1133/ffa8a8/fff5f5?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/820x1341/f8f9fa/495057?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/714x837/f8f9fa/495057?text=IMAGE%203',
        },
      ],
      categories: ['Pants', 'Sweaters', 'Jackets'],
      color: 'Black',
      sizes: ['M', 'Large', 'XS'],
    },
    {
      id: 25,
      title: 'Product #25',
      price: '$24000.99',
      sale_price: '$24000.99',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/436x366/f8f9fa/495057?text=PRODUCT%20%2325',
      images: [
        {
          url: 'https://via.placeholder.com/682x922/30d998/fff9db?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/909x757/ffa8a8/fff5f5?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/615x1409/f8f9fa/495057?text=IMAGE%203',
        },
      ],
      categories: ['Jackets', 'Jackets', 'Jackets'],
      color: 'Black',
      sizes: ['XL', 'Small', 'XS'],
    },
    {
      id: 26,
      title: 'Product #26',
      price: '$0.99',
      sale_price: '$0.7424999999999999',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/440x480/f8f9fa/495057?text=PRODUCT%20%2326',
      images: [
        {
          url: 'https://via.placeholder.com/514x889/30d998/fff9db?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/574x1279/30d998/fff9db?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/719x1051/202846/edf2ff?text=IMAGE%203',
        },
      ],
      categories: ['Caps', 'Sweaters', 'Socks'],
      color: 'Black',
      sizes: ['XL', 'Small', 'Small'],
    },
    {
      id: 27,
      title: 'Product #27',
      price: '$9.99',
      sale_price: '$9.99',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/281x355/30d998/fff9db?text=PRODUCT%20%2327',
      images: [
        {
          url: 'https://via.placeholder.com/1398x801/f8f9fa/495057?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/542x584/f8f9fa/495057?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/772x710/ffa8a8/fff5f5?text=IMAGE%203',
        },
      ],
      categories: ['Caps', 'Jackets', 'Caps'],
      color: 'Black',
      sizes: ['M', 'XS', 'M'],
    },
    {
      id: 28,
      title: 'Product #28',
      price: '$12.49',
      sale_price: '$12.49',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/257x456/202846/edf2ff?text=PRODUCT%20%2328',
      images: [
        {
          url: 'https://via.placeholder.com/677x1160/30d998/fff9db?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/768x917/f8f9fa/495057?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/939x696/ffa8a8/fff5f5?text=IMAGE%203',
        },
      ],
      categories: ['Jackets', 'Caps', 'Socks'],
      color: 'Yellow',
      sizes: ['Small', 'XL', 'M'],
    },
    {
      id: 29,
      title: 'Product #29',
      price: '$0.99',
      sale_price: '$0.99',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/267x318/ffa8a8/fff5f5?text=PRODUCT%20%2329',
      images: [
        {
          url: 'https://via.placeholder.com/631x1020/f8f9fa/495057?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/1189x1197/30d998/fff9db?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/649x678/30d998/fff9db?text=IMAGE%203',
        },
      ],
      categories: ['Pants', 'Pants', 'Pants'],
      color: 'Blue',
      sizes: ['XS', 'M', 'Small'],
    },
    {
      id: 30,
      title: 'Product #30',
      price: '$24000.99',
      sale_price: '$24000.99',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/379x363/202846/edf2ff?text=PRODUCT%20%2330',
      images: [
        {
          url: 'https://via.placeholder.com/794x484/ffa8a8/fff5f5?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/1073x1254/ffa8a8/fff5f5?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/1429x735/202846/edf2ff?text=IMAGE%203',
        },
      ],
      categories: ['Jackets', 'Sweaters', 'Pants'],
      color: 'Blue',
      sizes: ['XL', 'XS', 'Small'],
    },
    {
      id: 31,
      title: 'Product #31',
      price: '$0.99',
      sale_price: '$0.99',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/243x419/202846/edf2ff?text=PRODUCT%20%2331',
      images: [
        {
          url: 'https://via.placeholder.com/548x849/f8f9fa/495057?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/494x719/ffa8a8/fff5f5?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/547x1058/f8f9fa/495057?text=IMAGE%203',
        },
      ],
      categories: ['Sweaters', 'Socks', 'Jackets'],
      color: 'Black',
      sizes: ['XS', 'XL', 'XS'],
    },
    {
      id: 32,
      title: 'Product #32',
      price: '$1.99',
      sale_price: '$1.99',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/287x390/202846/edf2ff?text=PRODUCT%20%2332',
      images: [
        {
          url: 'https://via.placeholder.com/1280x722/f8f9fa/495057?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/753x662/f8f9fa/495057?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/537x738/f8f9fa/495057?text=IMAGE%203',
        },
      ],
      categories: ['Socks', 'Pants', 'Jackets'],
      color: 'Blue',
      sizes: ['M', 'Large', 'Small'],
    },
    {
      id: 33,
      title: 'Product #33',
      price: '$0.99',
      sale_price: '$0.99',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/340x474/f8f9fa/495057?text=PRODUCT%20%2333',
      images: [
        {
          url: 'https://via.placeholder.com/487x726/30d998/fff9db?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/1113x1128/ffa8a8/fff5f5?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/1192x1090/f8f9fa/495057?text=IMAGE%203',
        },
      ],
      categories: ['Pants', 'Jackets', 'Socks'],
      color: 'Black',
      sizes: ['Small', 'XS', 'Large'],
    },
    {
      id: 34,
      title: 'Product #34',
      price: '$9.99',
      sale_price: '$9.99',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/340x472/ffa8a8/fff5f5?text=PRODUCT%20%2334',
      images: [
        {
          url: 'https://via.placeholder.com/715x530/ffa8a8/fff5f5?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/1428x1220/202846/edf2ff?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/1258x866/30d998/fff9db?text=IMAGE%203',
        },
      ],
      categories: ['Pants', 'Pants', 'Pants'],
      color: 'Yellow',
      sizes: ['Small', 'XS', 'XL'],
    },
    {
      id: 35,
      title: 'Product #35',
      price: '$0.99',
      sale_price: '$0.99',
      in_stock: 'No',
      thumbnail:
        'https://via.placeholder.com/313x301/f8f9fa/495057?text=PRODUCT%20%2335',
      images: [
        {
          url: 'https://via.placeholder.com/1016x1431/30d998/fff9db?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/548x559/202846/edf2ff?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/1361x1410/30d998/fff9db?text=IMAGE%203',
        },
      ],
      categories: ['Socks', 'Caps', 'Caps'],
      color: 'Blue',
      sizes: ['XL', 'Small', 'Large'],
    },
    {
      id: 36,
      title: 'Product #36',
      price: '$9.99',
      sale_price: '$9.99',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/428x295/f8f9fa/495057?text=PRODUCT%20%2336',
      images: [
        {
          url: 'https://via.placeholder.com/1116x1362/202846/edf2ff?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/1439x1411/202846/edf2ff?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/1265x556/f8f9fa/495057?text=IMAGE%203',
        },
      ],
      categories: ['Jackets', 'Jackets', 'Jackets'],
      color: 'Blue',
      sizes: ['XS', 'XL', 'XL'],
    },
    {
      id: 37,
      title: 'Product #37',
      price: '$9.99',
      sale_price: '$9.99',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/359x394/ffa8a8/fff5f5?text=PRODUCT%20%2337',
      images: [
        {
          url: 'https://via.placeholder.com/1279x652/ffa8a8/fff5f5?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/980x1179/202846/edf2ff?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/1104x1376/30d998/fff9db?text=IMAGE%203',
        },
      ],
      categories: ['Sweaters', 'Pants', 'Caps'],
      color: 'White',
      sizes: ['XS', 'Small', 'M'],
    },
    {
      id: 38,
      title: 'Product #38',
      price: '$9.99',
      sale_price: '$7.4925',
      in_stock: 'No',
      thumbnail:
        'https://via.placeholder.com/292x251/f8f9fa/495057?text=PRODUCT%20%2338',
      images: [
        {
          url: 'https://via.placeholder.com/479x903/202846/edf2ff?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/549x646/ffa8a8/fff5f5?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/904x1187/30d998/fff9db?text=IMAGE%203',
        },
      ],
      categories: ['Jackets', 'Sweaters', 'Pants'],
      color: 'Blue',
      sizes: ['XL', 'M', 'XS'],
    },
    {
      id: 39,
      title: 'Product #39',
      price: '$9.99',
      sale_price: '$9.99',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/449x444/202846/edf2ff?text=PRODUCT%20%2339',
      images: [
        {
          url: 'https://via.placeholder.com/618x906/ffa8a8/fff5f5?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/794x1358/ffa8a8/fff5f5?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/1386x1313/30d998/fff9db?text=IMAGE%203',
        },
      ],
      categories: ['Jackets', 'Caps', 'Pants'],
      color: 'Yellow',
      sizes: ['XL', 'Large', 'M'],
    },
    {
      id: 40,
      title: 'Product #40',
      price: '$0.99',
      sale_price: '$0.99',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/348x449/202846/edf2ff?text=PRODUCT%20%2340',
      images: [
        {
          url: 'https://via.placeholder.com/693x1127/30d998/fff9db?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/1292x637/ffa8a8/fff5f5?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/1177x588/30d998/fff9db?text=IMAGE%203',
        },
      ],
      categories: ['Socks', 'Pants', 'Socks'],
      color: 'Blue',
      sizes: ['XS', 'XS', 'XS'],
    },
    {
      id: 41,
      title: 'Product #41',
      price: '$9.99',
      sale_price: '$9.99',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/299x458/ffa8a8/fff5f5?text=PRODUCT%20%2341',
      images: [
        {
          url: 'https://via.placeholder.com/1064x985/ffa8a8/fff5f5?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/825x900/30d998/fff9db?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/1349x1075/f8f9fa/495057?text=IMAGE%203',
        },
      ],
      categories: ['Sweaters', 'Socks', 'Jackets'],
      color: 'White',
      sizes: ['M', 'XS', 'XS'],
    },
    {
      id: 42,
      title: 'Product #42',
      price: '$24000.99',
      sale_price: '$24000.99',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/457x474/ffa8a8/fff5f5?text=PRODUCT%20%2342',
      images: [
        {
          url: 'https://via.placeholder.com/571x1128/ffa8a8/fff5f5?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/633x1091/f8f9fa/495057?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/581x1158/f8f9fa/495057?text=IMAGE%203',
        },
      ],
      categories: ['Pants', 'Sweaters', 'Jackets'],
      color: 'White',
      sizes: ['M', 'XS', 'M'],
    },
    {
      id: 43,
      title: 'Product #43',
      price: '$1.99',
      sale_price: '$1.99',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/408x302/ffa8a8/fff5f5?text=PRODUCT%20%2343',
      images: [
        {
          url: 'https://via.placeholder.com/539x727/30d998/fff9db?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/1102x723/30d998/fff9db?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/997x1024/202846/edf2ff?text=IMAGE%203',
        },
      ],
      categories: ['Jackets', 'Jackets', 'Sweaters'],
      color: 'Black',
      sizes: ['Large', 'Small', 'XS'],
    },
    {
      id: 44,
      title: 'Product #44',
      price: '$24000.99',
      sale_price: '$24000.99',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/437x386/f8f9fa/495057?text=PRODUCT%20%2344',
      images: [
        {
          url: 'https://via.placeholder.com/696x847/202846/edf2ff?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/620x1064/ffa8a8/fff5f5?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/1255x878/30d998/fff9db?text=IMAGE%203',
        },
      ],
      categories: ['Jackets', 'Jackets', 'Sweaters'],
      color: 'Yellow',
      sizes: ['M', 'Large', 'Small'],
    },
    {
      id: 45,
      title: 'Product #45',
      price: '$0.99',
      sale_price: '$0.99',
      in_stock: 'No',
      thumbnail:
        'https://via.placeholder.com/340x340/30d998/fff9db?text=PRODUCT%20%2345',
      images: [
        {
          url: 'https://via.placeholder.com/984x1165/30d998/fff9db?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/786x811/f8f9fa/495057?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/764x1044/ffa8a8/fff5f5?text=IMAGE%203',
        },
      ],
      categories: ['Sweaters', 'Caps', 'Socks'],
      color: 'Black',
      sizes: ['XL', 'XS', 'XS'],
    },
    {
      id: 46,
      title: 'Product #46',
      price: '$1.99',
      sale_price: '$1.99',
      in_stock: 'No',
      thumbnail:
        'https://via.placeholder.com/444x432/202846/edf2ff?text=PRODUCT%20%2346',
      images: [
        {
          url: 'https://via.placeholder.com/547x1093/ffa8a8/fff5f5?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/1248x1104/202846/edf2ff?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/513x832/ffa8a8/fff5f5?text=IMAGE%203',
        },
      ],
      categories: ['Sweaters', 'Sweaters', 'Sweaters'],
      color: 'Black',
      sizes: ['XS', 'XS', 'XS'],
    },
    {
      id: 47,
      title: 'Product #47',
      price: '$12.49',
      sale_price: '$9.3675',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/338x309/30d998/fff9db?text=PRODUCT%20%2347',
      images: [
        {
          url: 'https://via.placeholder.com/1005x714/ffa8a8/fff5f5?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/587x999/ffa8a8/fff5f5?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/1220x804/f8f9fa/495057?text=IMAGE%203',
        },
      ],
      categories: ['Sweaters', 'Jackets', 'Pants'],
      color: 'Blue',
      sizes: ['Large', 'XS', 'XS'],
    },
    {
      id: 48,
      title: 'Product #48',
      price: '$9.99',
      sale_price: '$7.4925',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/306x431/ffa8a8/fff5f5?text=PRODUCT%20%2348',
      images: [
        {
          url: 'https://via.placeholder.com/1392x1209/202846/edf2ff?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/1099x514/f8f9fa/495057?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/522x1160/30d998/fff9db?text=IMAGE%203',
        },
      ],
      categories: ['Jackets', 'Socks', 'Socks'],
      color: 'Black',
      sizes: ['Large', 'Small', 'Small'],
    },
    {
      id: 49,
      title: 'Product #49',
      price: '$12.49',
      sale_price: '$9.3675',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/464x464/202846/edf2ff?text=PRODUCT%20%2349',
      images: [
        {
          url: 'https://via.placeholder.com/729x714/f8f9fa/495057?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/968x931/f8f9fa/495057?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/1124x702/f8f9fa/495057?text=IMAGE%203',
        },
      ],
      categories: ['Socks', 'Jackets', 'Jackets'],
      color: 'Black',
      sizes: ['XL', 'Large', 'M'],
    },
    {
      id: 50,
      title: 'Product #50',
      price: '$12.49',
      sale_price: '$12.49',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/449x350/30d998/fff9db?text=PRODUCT%20%2350',
      images: [
        {
          url: 'https://via.placeholder.com/676x567/ffa8a8/fff5f5?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/870x956/202846/edf2ff?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/1217x774/202846/edf2ff?text=IMAGE%203',
        },
      ],
      categories: ['Socks', 'Sweaters', 'Socks'],
      color: 'Yellow',
      sizes: ['XL', 'XL', 'M'],
    },
    {
      id: 51,
      title: 'Product #51',
      price: '$1.99',
      sale_price: '$1.99',
      in_stock: 'No',
      thumbnail:
        'https://via.placeholder.com/280x398/ffa8a8/fff5f5?text=PRODUCT%20%2351',
      images: [
        {
          url: 'https://via.placeholder.com/1168x1247/30d998/fff9db?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/925x481/ffa8a8/fff5f5?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/565x717/ffa8a8/fff5f5?text=IMAGE%203',
        },
      ],
      categories: ['Caps', 'Caps', 'Pants'],
      color: 'Yellow',
      sizes: ['Small', 'XL', 'XL'],
    },
    {
      id: 52,
      title: 'Product #52',
      price: '$0.99',
      sale_price: '$0.99',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/470x253/202846/edf2ff?text=PRODUCT%20%2352',
      images: [
        {
          url: 'https://via.placeholder.com/743x1032/f8f9fa/495057?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/581x837/202846/edf2ff?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/1430x618/ffa8a8/fff5f5?text=IMAGE%203',
        },
      ],
      categories: ['Jackets', 'Sweaters', 'Sweaters'],
      color: 'White',
      sizes: ['Large', 'XL', 'XL'],
    },
    {
      id: 53,
      title: 'Product #53',
      price: '$1.99',
      sale_price: '$1.99',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/459x294/202846/edf2ff?text=PRODUCT%20%2353',
      images: [
        {
          url: 'https://via.placeholder.com/1351x1165/f8f9fa/495057?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/520x850/30d998/fff9db?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/1168x926/f8f9fa/495057?text=IMAGE%203',
        },
      ],
      categories: ['Socks', 'Socks', 'Caps'],
      color: 'White',
      sizes: ['XL', 'XL', 'Small'],
    },
    {
      id: 54,
      title: 'Product #54',
      price: '$12.49',
      sale_price: '$12.49',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/364x271/f8f9fa/495057?text=PRODUCT%20%2354',
      images: [
        {
          url: 'https://via.placeholder.com/629x589/202846/edf2ff?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/601x714/ffa8a8/fff5f5?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/804x1154/202846/edf2ff?text=IMAGE%203',
        },
      ],
      categories: ['Caps', 'Jackets', 'Caps'],
      color: 'Yellow',
      sizes: ['Small', 'M', 'XL'],
    },
    {
      id: 55,
      title: 'Product #55',
      price: '$24000.99',
      sale_price: '$24000.99',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/317x381/30d998/fff9db?text=PRODUCT%20%2355',
      images: [
        {
          url: 'https://via.placeholder.com/515x1328/202846/edf2ff?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/759x1083/30d998/fff9db?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/1395x1018/30d998/fff9db?text=IMAGE%203',
        },
      ],
      categories: ['Jackets', 'Socks', 'Sweaters'],
      color: 'White',
      sizes: ['XL', 'M', 'XL'],
    },
    {
      id: 56,
      title: 'Product #56',
      price: '$9.99',
      sale_price: '$9.99',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/476x414/f8f9fa/495057?text=PRODUCT%20%2356',
      images: [
        {
          url: 'https://via.placeholder.com/1004x565/ffa8a8/fff5f5?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/876x576/f8f9fa/495057?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/899x567/30d998/fff9db?text=IMAGE%203',
        },
      ],
      categories: ['Pants', 'Caps', 'Sweaters'],
      color: 'Blue',
      sizes: ['Large', 'M', 'Small'],
    },
    {
      id: 57,
      title: 'Product #57',
      price: '$12.49',
      sale_price: '$12.49',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/348x249/f8f9fa/495057?text=PRODUCT%20%2357',
      images: [
        {
          url: 'https://via.placeholder.com/885x1411/30d998/fff9db?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/490x1079/202846/edf2ff?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/1392x1173/f8f9fa/495057?text=IMAGE%203',
        },
      ],
      categories: ['Sweaters', 'Caps', 'Caps'],
      color: 'Black',
      sizes: ['M', 'XS', 'Large'],
    },
    {
      id: 58,
      title: 'Product #58',
      price: '$1.99',
      sale_price: '$1.99',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/416x431/202846/edf2ff?text=PRODUCT%20%2358',
      images: [
        {
          url: 'https://via.placeholder.com/1393x1248/202846/edf2ff?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/1438x1420/ffa8a8/fff5f5?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/1156x1135/f8f9fa/495057?text=IMAGE%203',
        },
      ],
      categories: ['Socks', 'Jackets', 'Socks'],
      color: 'White',
      sizes: ['M', 'XS', 'Small'],
    },
    {
      id: 59,
      title: 'Product #59',
      price: '$9.99',
      sale_price: '$9.99',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/464x260/f8f9fa/495057?text=PRODUCT%20%2359',
      images: [
        {
          url: 'https://via.placeholder.com/745x1267/30d998/fff9db?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/634x1361/f8f9fa/495057?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/1392x593/202846/edf2ff?text=IMAGE%203',
        },
      ],
      categories: ['Socks', 'Jackets', 'Sweaters'],
      color: 'White',
      sizes: ['Small', 'XS', 'M'],
    },
    {
      id: 60,
      title: 'Product #60',
      price: '$0.99',
      sale_price: '$0.99',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/384x382/202846/edf2ff?text=PRODUCT%20%2360',
      images: [
        {
          url: 'https://via.placeholder.com/1117x1349/202846/edf2ff?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/804x1380/30d998/fff9db?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/1149x1166/f8f9fa/495057?text=IMAGE%203',
        },
      ],
      categories: ['Socks', 'Sweaters', 'Jackets'],
      color: 'Blue',
      sizes: ['XS', 'Small', 'Large'],
    },
    {
      id: 61,
      title: 'Product #61',
      price: '$1.99',
      sale_price: '$1.99',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/423x293/202846/edf2ff?text=PRODUCT%20%2361',
      images: [
        {
          url: 'https://via.placeholder.com/1417x538/30d998/fff9db?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/1334x1122/ffa8a8/fff5f5?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/1361x603/f8f9fa/495057?text=IMAGE%203',
        },
      ],
      categories: ['Socks', 'Caps', 'Sweaters'],
      color: 'White',
      sizes: ['Small', 'XS', 'M'],
    },
    {
      id: 62,
      title: 'Product #62',
      price: '$9.99',
      sale_price: '$7.4925',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/442x446/f8f9fa/495057?text=PRODUCT%20%2362',
      images: [
        {
          url: 'https://via.placeholder.com/1223x497/f8f9fa/495057?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/1180x558/202846/edf2ff?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/1051x1236/202846/edf2ff?text=IMAGE%203',
        },
      ],
      categories: ['Caps', 'Jackets', 'Sweaters'],
      color: 'Yellow',
      sizes: ['Large', 'XS', 'XS'],
    },
    {
      id: 63,
      title: 'Product #63',
      price: '$9.99',
      sale_price: '$7.4925',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/258x410/202846/edf2ff?text=PRODUCT%20%2363',
      images: [
        {
          url: 'https://via.placeholder.com/1262x1049/30d998/fff9db?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/953x899/f8f9fa/495057?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/998x1069/ffa8a8/fff5f5?text=IMAGE%203',
        },
      ],
      categories: ['Pants', 'Jackets', 'Jackets'],
      color: 'Blue',
      sizes: ['Large', 'Small', 'Small'],
    },
    {
      id: 64,
      title: 'Product #64',
      price: '$12.49',
      sale_price: '$12.49',
      in_stock: 'No',
      thumbnail:
        'https://via.placeholder.com/448x394/30d998/fff9db?text=PRODUCT%20%2364',
      images: [
        {
          url: 'https://via.placeholder.com/1351x1121/202846/edf2ff?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/768x1174/202846/edf2ff?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/851x1374/30d998/fff9db?text=IMAGE%203',
        },
      ],
      categories: ['Sweaters', 'Jackets', 'Jackets'],
      color: 'Blue',
      sizes: ['Small', 'M', 'Small'],
    },
    {
      id: 65,
      title: 'Product #65',
      price: '$9.99',
      sale_price: '$7.4925',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/398x466/30d998/fff9db?text=PRODUCT%20%2365',
      images: [
        {
          url: 'https://via.placeholder.com/558x1437/f8f9fa/495057?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/602x660/30d998/fff9db?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/667x1105/30d998/fff9db?text=IMAGE%203',
        },
      ],
      categories: ['Caps', 'Caps', 'Pants'],
      color: 'White',
      sizes: ['M', 'XS', 'Large'],
    },
    {
      id: 66,
      title: 'Product #66',
      price: '$1.99',
      sale_price: '$1.99',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/364x355/ffa8a8/fff5f5?text=PRODUCT%20%2366',
      images: [
        {
          url: 'https://via.placeholder.com/744x605/30d998/fff9db?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/1036x1208/ffa8a8/fff5f5?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/1151x1346/202846/edf2ff?text=IMAGE%203',
        },
      ],
      categories: ['Sweaters', 'Socks', 'Socks'],
      color: 'Blue',
      sizes: ['M', 'Large', 'Small'],
    },
    {
      id: 67,
      title: 'Product #67',
      price: '$9.99',
      sale_price: '$9.99',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/328x409/ffa8a8/fff5f5?text=PRODUCT%20%2367',
      images: [
        {
          url: 'https://via.placeholder.com/497x1392/30d998/fff9db?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/527x1102/30d998/fff9db?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/1179x724/30d998/fff9db?text=IMAGE%203',
        },
      ],
      categories: ['Socks', 'Sweaters', 'Sweaters'],
      color: 'Blue',
      sizes: ['Small', 'XL', 'Large'],
    },
    {
      id: 68,
      title: 'Product #68',
      price: '$12.49',
      sale_price: '$12.49',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/332x382/30d998/fff9db?text=PRODUCT%20%2368',
      images: [
        {
          url: 'https://via.placeholder.com/810x598/f8f9fa/495057?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/1241x1065/f8f9fa/495057?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/683x1295/30d998/fff9db?text=IMAGE%203',
        },
      ],
      categories: ['Sweaters', 'Caps', 'Jackets'],
      color: 'Yellow',
      sizes: ['Large', 'XS', 'XL'],
    },
    {
      id: 69,
      title: 'Product #69',
      price: '$0.99',
      sale_price: '$0.7424999999999999',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/477x467/f8f9fa/495057?text=PRODUCT%20%2369',
      images: [
        {
          url: 'https://via.placeholder.com/1362x892/30d998/fff9db?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/943x731/f8f9fa/495057?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/1338x510/30d998/fff9db?text=IMAGE%203',
        },
      ],
      categories: ['Pants', 'Pants', 'Caps'],
      color: 'Blue',
      sizes: ['M', 'M', 'XL'],
    },
    {
      id: 70,
      title: 'Product #70',
      price: '$9.99',
      sale_price: '$9.99',
      in_stock: 'No',
      thumbnail:
        'https://via.placeholder.com/304x381/f8f9fa/495057?text=PRODUCT%20%2370',
      images: [
        {
          url: 'https://via.placeholder.com/997x548/30d998/fff9db?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/747x1141/30d998/fff9db?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/647x1131/ffa8a8/fff5f5?text=IMAGE%203',
        },
      ],
      categories: ['Sweaters', 'Caps', 'Caps'],
      color: 'White',
      sizes: ['Small', 'Small', 'M'],
    },
    {
      id: 71,
      title: 'Product #71',
      price: '$1.99',
      sale_price: '$1.99',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/255x321/ffa8a8/fff5f5?text=PRODUCT%20%2371',
      images: [
        {
          url: 'https://via.placeholder.com/741x738/f8f9fa/495057?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/1361x914/f8f9fa/495057?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/961x1412/202846/edf2ff?text=IMAGE%203',
        },
      ],
      categories: ['Socks', 'Caps', 'Jackets'],
      color: 'Yellow',
      sizes: ['M', 'M', 'M'],
    },
    {
      id: 72,
      title: 'Product #72',
      price: '$1.99',
      sale_price: '$1.99',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/466x436/f8f9fa/495057?text=PRODUCT%20%2372',
      images: [
        {
          url: 'https://via.placeholder.com/1031x562/f8f9fa/495057?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/500x1085/f8f9fa/495057?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/1176x764/f8f9fa/495057?text=IMAGE%203',
        },
      ],
      categories: ['Jackets', 'Caps', 'Jackets'],
      color: 'Blue',
      sizes: ['Small', 'XS', 'M'],
    },
    {
      id: 73,
      title: 'Product #73',
      price: '$9.99',
      sale_price: '$7.4925',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/420x412/f8f9fa/495057?text=PRODUCT%20%2373',
      images: [
        {
          url: 'https://via.placeholder.com/590x951/ffa8a8/fff5f5?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/579x763/30d998/fff9db?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/1168x1186/202846/edf2ff?text=IMAGE%203',
        },
      ],
      categories: ['Caps', 'Sweaters', 'Pants'],
      color: 'Black',
      sizes: ['XS', 'M', 'XL'],
    },
    {
      id: 74,
      title: 'Product #74',
      price: '$9.99',
      sale_price: '$9.99',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/266x274/f8f9fa/495057?text=PRODUCT%20%2374',
      images: [
        {
          url: 'https://via.placeholder.com/934x1155/30d998/fff9db?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/768x1417/ffa8a8/fff5f5?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/1109x975/30d998/fff9db?text=IMAGE%203',
        },
      ],
      categories: ['Sweaters', 'Jackets', 'Jackets'],
      color: 'Yellow',
      sizes: ['Small', 'Small', 'XL'],
    },
    {
      id: 75,
      title: 'Product #75',
      price: '$12.49',
      sale_price: '$9.3675',
      in_stock: 'No',
      thumbnail:
        'https://via.placeholder.com/274x297/ffa8a8/fff5f5?text=PRODUCT%20%2375',
      images: [
        {
          url: 'https://via.placeholder.com/769x1134/30d998/fff9db?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/1225x1098/ffa8a8/fff5f5?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/830x1146/ffa8a8/fff5f5?text=IMAGE%203',
        },
      ],
      categories: ['Pants', 'Jackets', 'Sweaters'],
      color: 'Black',
      sizes: ['Small', 'Small', 'Small'],
    },
    {
      id: 76,
      title: 'Product #76',
      price: '$24000.99',
      sale_price: '$18000.7425',
      in_stock: 'No',
      thumbnail:
        'https://via.placeholder.com/461x389/f8f9fa/495057?text=PRODUCT%20%2376',
      images: [
        {
          url: 'https://via.placeholder.com/1325x887/ffa8a8/fff5f5?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/659x1011/202846/edf2ff?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/674x993/f8f9fa/495057?text=IMAGE%203',
        },
      ],
      categories: ['Socks', 'Socks', 'Caps'],
      color: 'Black',
      sizes: ['XS', 'XS', 'Large'],
    },
    {
      id: 77,
      title: 'Product #77',
      price: '$0.99',
      sale_price: '$0.99',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/346x412/202846/edf2ff?text=PRODUCT%20%2377',
      images: [
        {
          url: 'https://via.placeholder.com/1100x892/f8f9fa/495057?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/1366x1259/f8f9fa/495057?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/1346x834/ffa8a8/fff5f5?text=IMAGE%203',
        },
      ],
      categories: ['Pants', 'Pants', 'Socks'],
      color: 'White',
      sizes: ['Large', 'Small', 'Small'],
    },
    {
      id: 78,
      title: 'Product #78',
      price: '$24000.99',
      sale_price: '$18000.7425',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/313x474/202846/edf2ff?text=PRODUCT%20%2378',
      images: [
        {
          url: 'https://via.placeholder.com/887x533/ffa8a8/fff5f5?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/1062x1098/202846/edf2ff?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/943x1195/ffa8a8/fff5f5?text=IMAGE%203',
        },
      ],
      categories: ['Sweaters', 'Jackets', 'Caps'],
      color: 'Yellow',
      sizes: ['Large', 'M', 'Small'],
    },
    {
      id: 79,
      title: 'Product #79',
      price: '$24000.99',
      sale_price: '$24000.99',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/328x266/202846/edf2ff?text=PRODUCT%20%2379',
      images: [
        {
          url: 'https://via.placeholder.com/857x490/30d998/fff9db?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/700x1298/ffa8a8/fff5f5?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/659x670/202846/edf2ff?text=IMAGE%203',
        },
      ],
      categories: ['Pants', 'Pants', 'Sweaters'],
      color: 'White',
      sizes: ['Small', 'M', 'XS'],
    },
    {
      id: 80,
      title: 'Product #80',
      price: '$24000.99',
      sale_price: '$18000.7425',
      in_stock: 'No',
      thumbnail:
        'https://via.placeholder.com/245x343/f8f9fa/495057?text=PRODUCT%20%2380',
      images: [
        {
          url: 'https://via.placeholder.com/1031x888/f8f9fa/495057?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/942x1436/202846/edf2ff?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/585x558/f8f9fa/495057?text=IMAGE%203',
        },
      ],
      categories: ['Socks', 'Pants', 'Socks'],
      color: 'Blue',
      sizes: ['Small', 'XL', 'XS'],
    },
    {
      id: 81,
      title: 'Product #81',
      price: '$9.99',
      sale_price: '$7.4925',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/384x380/30d998/fff9db?text=PRODUCT%20%2381',
      images: [
        {
          url: 'https://via.placeholder.com/733x940/202846/edf2ff?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/621x931/f8f9fa/495057?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/1435x1021/202846/edf2ff?text=IMAGE%203',
        },
      ],
      categories: ['Sweaters', 'Socks', 'Pants'],
      color: 'Blue',
      sizes: ['XL', 'XS', 'XL'],
    },
    {
      id: 82,
      title: 'Product #82',
      price: '$9.99',
      sale_price: '$9.99',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/303x305/ffa8a8/fff5f5?text=PRODUCT%20%2382',
      images: [
        {
          url: 'https://via.placeholder.com/937x1253/ffa8a8/fff5f5?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/1135x1145/30d998/fff9db?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/1357x892/ffa8a8/fff5f5?text=IMAGE%203',
        },
      ],
      categories: ['Pants', 'Caps', 'Jackets'],
      color: 'Blue',
      sizes: ['Large', 'Small', 'M'],
    },
    {
      id: 83,
      title: 'Product #83',
      price: '$1.99',
      sale_price: '$1.99',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/300x299/ffa8a8/fff5f5?text=PRODUCT%20%2383',
      images: [
        {
          url: 'https://via.placeholder.com/1129x1180/30d998/fff9db?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/707x774/f8f9fa/495057?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/706x1189/ffa8a8/fff5f5?text=IMAGE%203',
        },
      ],
      categories: ['Jackets', 'Sweaters', 'Sweaters'],
      color: 'White',
      sizes: ['Large', 'Large', 'Small'],
    },
    {
      id: 84,
      title: 'Product #84',
      price: '$1.99',
      sale_price: '$1.4925',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/272x259/ffa8a8/fff5f5?text=PRODUCT%20%2384',
      images: [
        {
          url: 'https://via.placeholder.com/667x1000/f8f9fa/495057?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/1229x1177/f8f9fa/495057?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/689x730/f8f9fa/495057?text=IMAGE%203',
        },
      ],
      categories: ['Sweaters', 'Pants', 'Caps'],
      color: 'White',
      sizes: ['XS', 'M', 'Small'],
    },
    {
      id: 85,
      title: 'Product #85',
      price: '$12.49',
      sale_price: '$9.3675',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/387x301/ffa8a8/fff5f5?text=PRODUCT%20%2385',
      images: [
        {
          url: 'https://via.placeholder.com/1267x639/30d998/fff9db?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/1214x997/202846/edf2ff?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/628x509/202846/edf2ff?text=IMAGE%203',
        },
      ],
      categories: ['Socks', 'Jackets', 'Socks'],
      color: 'Black',
      sizes: ['Large', 'XS', 'Large'],
    },
    {
      id: 86,
      title: 'Product #86',
      price: '$9.99',
      sale_price: '$9.99',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/252x440/30d998/fff9db?text=PRODUCT%20%2386',
      images: [
        {
          url: 'https://via.placeholder.com/551x1354/ffa8a8/fff5f5?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/533x1344/f8f9fa/495057?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/798x1368/ffa8a8/fff5f5?text=IMAGE%203',
        },
      ],
      categories: ['Pants', 'Pants', 'Socks'],
      color: 'Black',
      sizes: ['Small', 'M', 'XL'],
    },
    {
      id: 87,
      title: 'Product #87',
      price: '$24000.99',
      sale_price: '$24000.99',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/431x362/f8f9fa/495057?text=PRODUCT%20%2387',
      images: [
        {
          url: 'https://via.placeholder.com/1040x535/ffa8a8/fff5f5?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/1010x1069/30d998/fff9db?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/1194x1180/30d998/fff9db?text=IMAGE%203',
        },
      ],
      categories: ['Sweaters', 'Jackets', 'Pants'],
      color: 'Black',
      sizes: ['Small', 'XS', 'XS'],
    },
    {
      id: 88,
      title: 'Product #88',
      price: '$24000.99',
      sale_price: '$24000.99',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/311x435/f8f9fa/495057?text=PRODUCT%20%2388',
      images: [
        {
          url: 'https://via.placeholder.com/1279x969/30d998/fff9db?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/1231x1314/f8f9fa/495057?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/554x735/f8f9fa/495057?text=IMAGE%203',
        },
      ],
      categories: ['Socks', 'Caps', 'Jackets'],
      color: 'Yellow',
      sizes: ['Small', 'XL', 'Small'],
    },
    {
      id: 89,
      title: 'Product #89',
      price: '$1.99',
      sale_price: '$1.4925',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/280x326/202846/edf2ff?text=PRODUCT%20%2389',
      images: [
        {
          url: 'https://via.placeholder.com/979x1407/30d998/fff9db?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/1055x813/30d998/fff9db?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/1160x892/30d998/fff9db?text=IMAGE%203',
        },
      ],
      categories: ['Pants', 'Socks', 'Caps'],
      color: 'Black',
      sizes: ['Large', 'M', 'Small'],
    },
    {
      id: 90,
      title: 'Product #90',
      price: '$0.99',
      sale_price: '$0.99',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/264x453/f8f9fa/495057?text=PRODUCT%20%2390',
      images: [
        {
          url: 'https://via.placeholder.com/1416x1387/202846/edf2ff?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/703x568/202846/edf2ff?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/561x489/30d998/fff9db?text=IMAGE%203',
        },
      ],
      categories: ['Pants', 'Jackets', 'Caps'],
      color: 'White',
      sizes: ['Small', 'XS', 'Small'],
    },
    {
      id: 91,
      title: 'Product #91',
      price: '$0.99',
      sale_price: '$0.99',
      in_stock: 'No',
      thumbnail:
        'https://via.placeholder.com/298x329/30d998/fff9db?text=PRODUCT%20%2391',
      images: [
        {
          url: 'https://via.placeholder.com/770x1238/f8f9fa/495057?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/843x519/202846/edf2ff?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/479x1042/ffa8a8/fff5f5?text=IMAGE%203',
        },
      ],
      categories: ['Caps', 'Jackets', 'Socks'],
      color: 'Black',
      sizes: ['Large', 'XS', 'XS'],
    },
    {
      id: 92,
      title: 'Product #92',
      price: '$9.99',
      sale_price: '$9.99',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/368x476/f8f9fa/495057?text=PRODUCT%20%2392',
      images: [
        {
          url: 'https://via.placeholder.com/1280x710/30d998/fff9db?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/1339x1015/30d998/fff9db?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/1297x1190/30d998/fff9db?text=IMAGE%203',
        },
      ],
      categories: ['Jackets', 'Sweaters', 'Jackets'],
      color: 'White',
      sizes: ['Small', 'Small', 'XS'],
    },
    {
      id: 93,
      title: 'Product #93',
      price: '$9.99',
      sale_price: '$9.99',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/369x249/30d998/fff9db?text=PRODUCT%20%2393',
      images: [
        {
          url: 'https://via.placeholder.com/843x1410/ffa8a8/fff5f5?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/1416x924/30d998/fff9db?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/1216x617/ffa8a8/fff5f5?text=IMAGE%203',
        },
      ],
      categories: ['Caps', 'Jackets', 'Sweaters'],
      color: 'Blue',
      sizes: ['Small', 'M', 'XS'],
    },
    {
      id: 94,
      title: 'Product #94',
      price: '$9.99',
      sale_price: '$9.99',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/476x254/30d998/fff9db?text=PRODUCT%20%2394',
      images: [
        {
          url: 'https://via.placeholder.com/924x1028/ffa8a8/fff5f5?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/983x836/f8f9fa/495057?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/1210x1360/30d998/fff9db?text=IMAGE%203',
        },
      ],
      categories: ['Sweaters', 'Caps', 'Sweaters'],
      color: 'Yellow',
      sizes: ['M', 'Large', 'Large'],
    },
    {
      id: 95,
      title: 'Product #95',
      price: '$0.99',
      sale_price: '$0.99',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/403x284/f8f9fa/495057?text=PRODUCT%20%2395',
      images: [
        {
          url: 'https://via.placeholder.com/960x523/f8f9fa/495057?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/962x1408/202846/edf2ff?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/514x960/f8f9fa/495057?text=IMAGE%203',
        },
      ],
      categories: ['Pants', 'Jackets', 'Pants'],
      color: 'Black',
      sizes: ['M', 'XL', 'Large'],
    },
    {
      id: 96,
      title: 'Product #96',
      price: '$0.99',
      sale_price: '$0.99',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/306x362/202846/edf2ff?text=PRODUCT%20%2396',
      images: [
        {
          url: 'https://via.placeholder.com/552x915/30d998/fff9db?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/1423x1428/f8f9fa/495057?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/944x1264/ffa8a8/fff5f5?text=IMAGE%203',
        },
      ],
      categories: ['Jackets', 'Socks', 'Caps'],
      color: 'Yellow',
      sizes: ['XL', 'Large', 'Small'],
    },
    {
      id: 97,
      title: 'Product #97',
      price: '$12.49',
      sale_price: '$9.3675',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/301x360/202846/edf2ff?text=PRODUCT%20%2397',
      images: [
        {
          url: 'https://via.placeholder.com/1277x527/f8f9fa/495057?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/1298x513/30d998/fff9db?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/1086x1034/202846/edf2ff?text=IMAGE%203',
        },
      ],
      categories: ['Sweaters', 'Pants', 'Jackets'],
      color: 'White',
      sizes: ['XL', 'Large', 'Small'],
    },
    {
      id: 98,
      title: 'Product #98',
      price: '$24000.99',
      sale_price: '$24000.99',
      in_stock: 'No',
      thumbnail:
        'https://via.placeholder.com/264x260/202846/edf2ff?text=PRODUCT%20%2398',
      images: [
        {
          url: 'https://via.placeholder.com/1402x584/f8f9fa/495057?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/1309x870/202846/edf2ff?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/694x1255/f8f9fa/495057?text=IMAGE%203',
        },
      ],
      categories: ['Jackets', 'Pants', 'Pants'],
      color: 'White',
      sizes: ['Large', 'XL', 'XL'],
    },
    {
      id: 99,
      title: 'Product #99',
      price: '$12.49',
      sale_price: '$12.49',
      in_stock: 'Yes',
      thumbnail:
        'https://via.placeholder.com/317x398/f8f9fa/495057?text=PRODUCT%20%2399',
      images: [
        {
          url: 'https://via.placeholder.com/1205x513/202846/edf2ff?text=IMAGE%201',
        },
        {
          url: 'https://via.placeholder.com/915x1297/202846/edf2ff?text=IMAGE%202',
        },
        {
          url: 'https://via.placeholder.com/1159x1109/ffa8a8/fff5f5?text=IMAGE%203',
        },
      ],
      categories: ['Pants', 'Socks', 'Caps'],
      color: 'Blue',
      sizes: ['M', 'Small', 'XS'],
    },
  ];
}

module.exports = { products };
